import { createAction, props } from '@ngrx/store';
import { Gebruiker } from './gebruiker.types';

// https://ngrx.io/guide/store/actions
// Actions are one of the main building blocks in NgRx. Actions express unique events that happen throughout your application.
// From user interaction with the page, external interaction through network requests, and direct interaction with device APIs, these
// and more events are described with actions.

// - user interaction en device API interactions.

export const loadGebruikersSuccess = createAction('[Gebruikers] Loading Success', props<{ gebruikers: Gebruiker[] }>());
export const loadGebruikersStart = createAction('[Gebruikers] Loading Start');
