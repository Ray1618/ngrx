import { Gebruiker } from './gebruiker.types';

// de initial store state van gebruikers

export const initialState: GebruikerState = {
  gebruikers: [],
  loading: false,
};

export interface GebruikerState {
  gebruikers: Gebruiker[];
  loading: boolean;
}
