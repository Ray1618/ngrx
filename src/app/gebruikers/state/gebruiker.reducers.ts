import { Action, createReducer, on } from '@ngrx/store';
import { loadGebruikersStart, loadGebruikersSuccess } from './gebruiker.actions';
import { initialState, GebruikerState } from './gebruiker.state';
import { AppState } from './gebruiker.selectors';

// https://ngrx.io/guide/store/reducers
// Reducers in NgRx are responsible for handling transitions from one state to the next state in your application.
// Reducer functions handle these transitions by determining which actions to handle based on the action's type.

const reducers = createReducer(
  initialState,
  on(loadGebruikersStart, (state) => ({...state, gebruikers: [], loading: true})),
  on(loadGebruikersSuccess, (state, {gebruikers}) => ({...state, gebruikers, loading: false})),
);

export function gebruikersReducer(state: GebruikerState | undefined, action: Action) {
  return reducers(state, action);
}
