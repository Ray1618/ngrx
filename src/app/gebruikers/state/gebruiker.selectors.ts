import { createFeatureSelector, createSelector } from '@ngrx/store';
import { GebruikerState } from './gebruiker.state';

// https://ngrx.io/guide/store/selectors
/*
  Selectors are pure functions used for obtaining slices of store state. @ngrx/store provides a few helper functions for optimizing
  this selection. Selectors provide many features when selecting slices of state:

  Portability
  Memoization
  Composition
  Testability
  Type Safety
*/

export interface AppState {
  gebruikers: GebruikerState;
}

const selectFeature = createFeatureSelector<AppState, GebruikerState>('gebruikers');

export const selectGebruikersData = createSelector(
  selectFeature,
    state => ({
      gebruikers: state.gebruikers,
      loading: state.loading,
    }),
);
