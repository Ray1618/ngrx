import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { GebruikerService } from './gebruiker.service';
import { loadGebruikersStart, loadGebruikersSuccess } from './gebruiker.actions';
import { map, switchMap } from 'rxjs/operators';

// https://ngrx.io/guide/effects
/*
  Effects are an RxJS powered side effect model for Store. Effects use streams to provide new sources of actions to
  reduce state based on external interactions such as network requests, web socket messages and time-based events.

  Introduction
  In a service-based Angular application, components are responsible for interacting with external resources directly through services.
  Instead, effects provide a way to interact with those services and isolate them from the components. Effects are where you handle tasks
  such as fetching data, long-running tasks that produce multiple events, and other external interactions where your components
  don't need explicit knowledge of these interactions.
*/

@Injectable()
export class GebruikerEffects {
  constructor(
    private actions$: Actions,
    private gebruikerService: GebruikerService,
  ) {}

  loadGebruikers$ = createEffect(() => this.actions$.pipe(
    ofType(loadGebruikersStart),
    switchMap(() => this.gebruikerService.loadGebruikers()),
    map((gebruikers) => loadGebruikersSuccess({gebruikers})),
  ));
}
